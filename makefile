CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic

.PHONY : all
all : WTX


WTX : romandigitconverter.cpp numberconversion.o
	$(CXX) $(CXXFLAGS) -o $@ $^

numberconversion.o : numberconversion.cpp numberconversion.h
	 $(CXX) $(CXXFLAGS) -c $<


.PHONY : CLEAN
clean :
	$(RM) *.o
	$(RM) WTX
